package pl.tobiasz.porebski.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import pl.tobiasz.porebski.database.TableHelper.TableSchema;

public class MyDataBaseReader {
	
	private static String longProjection[] = {
		TableSchema.COLUMN_NAME_TASK_ID,
		TableSchema.COLUMN_NAME_TITLE,
		TableSchema.COLUMN_NAME_DESCRIPTION,
		TableSchema.COULUMN_NAME_CRATED,
		TableSchema.COULUMN_NAME_TIME_END,
		TableSchema.COULUMN_NAME_ICON_URL
	};
	
	private static String shortProjection [] = {
			TableSchema.COLUMN_NAME_TASK_ID,
			TableSchema.COLUMN_NAME_TITLE,
//			TableSchema.COULUMN_NAME_CRATED,
			TableSchema.COULUMN_NAME_TIME_END,
			TableSchema.COULUMN_NAME_ICON_URL
			
	};
	
	private static String SORT_ORDER = "INC";
	
	protected static Cursor takeLongInformationCursor(SQLiteDatabase db, String rowId){

		String selectionArgs[] = {rowId};
		
		Cursor c = db.query(
				TableSchema.TABLE_NAME,					  // The table to query
				longProjection,                           // The columns to return
				TableSchema.COLUMN_NAME_TASK_ID,          // The columns for the WHERE clause
				selectionArgs,				              // The values for the WHERE clause
				null,                                     // don't group the rows
				null,                                     // don't filter by row groups
				TableSchema.COLUMN_NAME_TASK_ID           // The sort order
	    );
		
		setLastID(c);
		
		return c;
	}
	
	protected static Cursor takeShortInformationCursor(SQLiteDatabase db){
		
		Cursor c = db.query(
				TableSchema.TABLE_NAME,					// The table to query
			    shortProjection,                        // The columns to return
			    null,          							// The columns for the WHERE clause
			    null,				              		// The values for the WHERE clause
			    null,                                   // don't group the rows
			    null,                                   // don't filter by row groups
			    TableSchema.COLUMN_NAME_TASK_ID         // The sort order
			    );
		
		setLastID(c);
		
		return c;
	}
	
	private static void setLastID(Cursor c){
		
		// Ustaw odpowiednie ID dla MyDstabaseWriter
		
		if(SORT_ORDER == "DEC"){
			c.moveToFirst();
		}else{
			c.moveToFirst();
		}
		
		try{
			MyDatabaseWriter.setLastID(c.getLong(0));
		}catch(Exception e){
			MyDatabaseWriter.setLastID(-1);
		}
		
	}
	

}
