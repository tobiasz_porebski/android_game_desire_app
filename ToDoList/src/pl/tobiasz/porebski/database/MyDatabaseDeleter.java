package pl.tobiasz.porebski.database;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import pl.tobiasz.porebski.database.TableHelper.TableSchema;

public class MyDatabaseDeleter {

	
	protected static boolean deleteTask(SQLiteDatabase db, long id){
		
		// Define 'where' part of query.
		String selection = TableSchema.COLUMN_NAME_TASK_ID + " = ";
		// Specify arguments in placeholder order.
		String[] selectionArgs = { String.valueOf(id) };
		// Issue SQL statement.
		int rowsAffacted = db.delete(TableSchema.TABLE_NAME, selection, selectionArgs);
		
		
		if(rowsAffacted > 0)
			return true;
		else
			return false;
	}
	
	protected static boolean deleteAllTasks( SQLiteDatabase db) {
		
		try{
			db.delete(TableSchema.TABLE_NAME, null, null);
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
		
	
}
