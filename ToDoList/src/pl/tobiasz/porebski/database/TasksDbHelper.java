package pl.tobiasz.porebski.database;

import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import pl.tobiasz.porebski.todolist.R;
import android.database.sqlite.SQLiteOpenHelper;

public class TasksDbHelper extends SQLiteOpenHelper{
	
	private Context context;
	private SQLiteDatabase tasksDB;
	
	public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "TasksGD.db";
    


	public TasksDbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TableHelper.SQL_CREATE_ENTRIES);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		//Wyczy�� baz� i stw�rz tabele na nowo
		db.execSQL(TableHelper.SQL_DELETE_ENTRIES);
		onCreate(db);
		
	}
	
	public boolean open(){
		
		try{
			tasksDB = this.getWritableDatabase();
		}catch(SQLException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void close(){
		
		if(tasksDB != null){
			tasksDB.close();
		}
		
	}
	
	public Cursor getAllTasksForListView(){
		return MyDataBaseReader.takeShortInformationCursor(tasksDB);
	}
	
	public Cursor getSpecificTaskInformation(long rowId){
		return MyDataBaseReader.takeLongInformationCursor(tasksDB, String.valueOf(rowId));
	}
		
	/*
	 * TODO: Put some data into databe gor debugging
	 */
	public void fillDbWithSomeData(){
		
		Calendar rightNow = Calendar.getInstance();
		Date tmpDate = new Date(rightNow.getTimeInMillis() + 500000);
		String imageUri = "" + R.drawable.ic_launcher;
		
		MyDatabaseWriter.putNewTask(tasksDB, "Zadanie A", "id� do a", rightNow.getTime(), tmpDate, imageUri);
		MyDatabaseWriter.putNewTask(tasksDB, "Zadanie B", "zr�b b", rightNow.getTime(), tmpDate, imageUri);
		MyDatabaseWriter.putNewTask(tasksDB, "Zadanie C", "pami�taj o C", rightNow.getTime(), tmpDate, imageUri);
		MyDatabaseWriter.putNewTask(tasksDB, "Zadanie D", "D - prezent", rightNow.getTime(), tmpDate, imageUri);
		MyDatabaseWriter.putNewTask(tasksDB, "Zadanie E", "id� do e", rightNow.getTime(), tmpDate, imageUri);
		MyDatabaseWriter.putNewTask(tasksDB, "Zadanie F", "id� do f", rightNow.getTime(), tmpDate, imageUri);
		MyDatabaseWriter.putNewTask(tasksDB, "Zadanie F2", "id� do f2", rightNow.getTime(), tmpDate, imageUri);
		MyDatabaseWriter.putNewTask(tasksDB, "Zadanie F3", "id� do f3", rightNow.getTime(), tmpDate, imageUri);

	}
	
	public void deleteAllTasks(){
		//MyDatabaseDeleter.deleteAllTasks(tasksDB);
		onUpgrade(tasksDB, DATABASE_VERSION, DATABASE_VERSION + 1);
	}
	

}
