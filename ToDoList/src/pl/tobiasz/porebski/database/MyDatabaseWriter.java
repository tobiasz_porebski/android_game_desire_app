package pl.tobiasz.porebski.database;

import java.util.Date;
import android.database.sqlite.SQLiteDatabase;
import pl.tobiasz.porebski.database.TableHelper.TableSchema;
import android.content.ContentValues; 

public class MyDatabaseWriter {

	private static long lastID;
	
	protected static  void setLastID(long id){
		if(id >= 0)
			lastID = id;
	}
	
	
	protected static boolean putNewTask(
			SQLiteDatabase db,
			String title,
			String description,
			Date created,
			Date timeEnd,
			String urlToImage){
		
		// prepare tmie in miliseconds
		long createdTime = created.getTime();
		long endTime = timeEnd.getTime();
		
		// create new map of given values
		ContentValues values = new ContentValues();
		values.put(TableSchema.COLUMN_NAME_TASK_ID, lastID + 1);
		values.put(TableSchema.COLUMN_NAME_TITLE, title);
		values.put(TableSchema.COLUMN_NAME_DESCRIPTION, description);
		values.put(TableSchema.COULUMN_NAME_CRATED, createdTime);
		values.put(TableSchema.COULUMN_NAME_TIME_END, endTime);
		values.put(TableSchema.COULUMN_NAME_ICON_URL, urlToImage);

		
		
		long insertedRowID = db.insert(
				TableSchema.TABLE_NAME, 
				null,						// null because there is no option for passing values to be empty.
				values);
		
		
		// check if operation was successfull
		if(insertedRowID == -1)
			return false;
		else{
			//set new value to lastID
			lastID = insertedRowID;
			
			return true;
		}
		
	}
}
