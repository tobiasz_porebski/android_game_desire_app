package pl.tobiasz.porebski.database;

import android.provider.BaseColumns;

public final class TableHelper {

	    public TableHelper() {}

	    /* Inner class that defines the table contents */
	    public static abstract class TableSchema implements BaseColumns {
	        public static final String TABLE_NAME = "tasks";
	        public static final String COLUMN_NAME_TASK_ID = "_id";
	        public static final String COLUMN_NAME_TITLE = "title";
	        public static final String COLUMN_NAME_DESCRIPTION = "description";
	        public static final String COULUMN_NAME_CRATED = "time_crated";
	        public static final String COULUMN_NAME_TIME_END = "time_end";
	        public static final String COULUMN_NAME_ICON_URL = "icon_url";

	        
	    }
	    
	    private static final String TEXT_TYPE = " TEXT";
	    private static final String DATE_TYPE = " INTEGER";
	    private static final String COMMA_SEP = ",";
	    
	    public static final String SQL_CREATE_ENTRIES =
	        "CREATE TABLE " + TableSchema.TABLE_NAME + " (" +
	        TableSchema.COLUMN_NAME_TASK_ID + " INTEGER PRIMARY KEY," +
	        TableSchema.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
	        TableSchema.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
	        TableSchema.COULUMN_NAME_CRATED + DATE_TYPE + COMMA_SEP +
	        TableSchema.COULUMN_NAME_TIME_END + DATE_TYPE + COMMA_SEP +
	        TableSchema.COULUMN_NAME_ICON_URL + TEXT_TYPE + 
	        
	        " )";

	    public static final String SQL_DELETE_ENTRIES =
	        "DROP TABLE IF EXISTS " + TableSchema.TABLE_NAME;
	
	
}
