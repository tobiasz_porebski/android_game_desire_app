package pl.tobiasz.porebski.database;

import java.util.Date;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import pl.tobiasz.porebski.database.TableHelper.TableSchema;

public class MyDatabseUpdater {

	/*
	 * NOTE : In the table values "created" nad "id" cannot be modified.
	 */
	
	// ---------- Main update method ----------
	
	private static boolean update(SQLiteDatabase db, long id, ContentValues values){
		
		// Which row to update, based on the ID
		String selection = TableSchema.COLUMN_NAME_TASK_ID + " = ";
		String[] selectionArgs = { String.valueOf( id ) };

		int count = db.update(
		    TableSchema.TABLE_NAME,
		    values,
		    selection,
		    selectionArgs);
		
		//check if operation was successfull : exactly one row was affected
		if(count == 1)
			return true;
		else
			return false;
	}
	
	
	
	// ---------- Group update ----------
	
	protected static boolean updateTaskInformation(
			SQLiteDatabase db,
			long id,
			String new_title,
			String new_description,
			Date new_timeEnd,
			String new_imgae_url){
		
		//prepare time in miliseconds
		long deadlineTime = new_timeEnd.getTime();
		
		// New value for columns
		ContentValues values = new ContentValues();
		values.put(TableSchema.COLUMN_NAME_TITLE, new_title);
		values.put(TableSchema.COLUMN_NAME_DESCRIPTION, new_description);
		values.put(TableSchema.COULUMN_NAME_TIME_END, deadlineTime);
		values.put(TableSchema.COULUMN_NAME_ICON_URL, new_imgae_url);
	
		return update(db,id,values);
	}
	
	// ---------- Title update ----------
	
	protected static boolean updateTitle(SQLiteDatabase db, long id, String new_title){
		
		ContentValues values = new ContentValues();
		values.put(TableSchema.COLUMN_NAME_TITLE, new_title);
		
		return update(db, id, values);
	}
	
	// ---------- Description update ----------
	
	protected static boolean updateDescription(SQLiteDatabase db, long id, String new_description){
		
		ContentValues values = new ContentValues();
		values.put(TableSchema.COLUMN_NAME_DESCRIPTION, new_description);
		
		return update(db, id, values);
	}
	
	// ---------- End time update ----------
	
	protected static boolean updateDeadline(SQLiteDatabase db, long id, Date new_deadline){
		
		long deadlineTime = new_deadline.getTime();
	
		ContentValues values = new ContentValues();
		values.put(TableSchema.COULUMN_NAME_TIME_END, deadlineTime);
		
		return update(db, id, values);
	}
	
	// ---------- Icon update ----------

	protected static boolean updateIcon(SQLiteDatabase db, long id, String new_url_image){
	
	ContentValues values = new ContentValues();
	values.put(TableSchema.COULUMN_NAME_ICON_URL, new_url_image);
	
	return update(db, id, values);
}
	
	
}
