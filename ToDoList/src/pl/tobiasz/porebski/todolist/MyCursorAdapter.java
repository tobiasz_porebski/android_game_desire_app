package pl.tobiasz.porebski.todolist;


import java.util.Date;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
//import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import pl.tobiasz.porebski.database.TableHelper.TableSchema;

public class MyCursorAdapter extends SimpleCursorAdapter{

	private Cursor mainCursor;
	private Context mContext;
	private int layout;
	private final LayoutInflater inflater;
	
	@SuppressWarnings("deprecation")
	public MyCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
		super(context, layout, c, from, to);
		
		mContext = context;
		this.inflater = LayoutInflater.from(context);
//		configureBindingOptions();
		mainCursor = c;
		
	}
	/*
	@Override
    public View newView (Context context, Cursor cursor, ViewGroup parent) {
            return inflater.inflate(layout, null);
    }
	*/
	

	
	@Override
	public void bindView(View view, Context contx, Cursor c) {
//		super.bindView(view, contx, c);
		
		// My presonal binding
		TextView titleV = (TextView) view.findViewById(R.id.firstLine);
		TextView timeEndV = (TextView) view.findViewById(R.id.secondLine);
		ImageView iconV = (ImageView) view.findViewById(R.id.icon);
		
		//Title
		titleV.setText(c.getString(c.getColumnIndex(TableSchema.COLUMN_NAME_TITLE)));
		
		// Deadline
		long milisecondsToEnd = c.getLong(c.getColumnIndex(TableSchema.COULUMN_NAME_TIME_END));
		Date deadlieTime = new Date(milisecondsToEnd);
		timeEndV.setText(deadlieTime.toString());
		
		//Image
		String imageUri = c.getString(c.getColumnIndex(TableSchema.COULUMN_NAME_ICON_URL));
		if(!imageUri.equals(""))
		{
			iconV.setImageResource(Integer.valueOf(imageUri));;
//			iconV.setImageURI(Uri.parse(imageUri));
		}
		super.bindView(view, contx, c);
	}
	
/*
	private void configureBindingOptions(){
		
		ViewBinder vb = new ViewBinder() {
			
			@Override
			public boolean setViewValue(View view, Cursor cursor, int index) {
				
				// only DATE and IMAGE data have to be handle differently
				// in pasing Cursor task's ending time will be under index : 1
				// and image uri under index : 2
				
				if( index == 1){
					
					long milisecondsToEnd = cursor.getLong(index);
					Date deadlieTime = new Date(milisecondsToEnd);
					//Date timeLeft = new Date();
					//timeLeft.getDay()
					TextView deadlineTextView = (TextView)view.findViewById(R.id.secondLine);
					deadlineTextView.setText(deadlieTime.toString());
					
					return true;
				}
				
				if( index == 2 ){
					
					String iconSrcPath = cursor.getString(index);
					
					// if icon was set
					if(iconSrcPath.equals("")){
						ImageView imgView = (ImageView)view.findViewById(R.id.icon);
						imgView.setImageURI(Uri.parse(iconSrcPath));
					}
					return true;
				}
				
				return false;
			}
		};
		
		//Set new ViewBinder
		setViewBinder(vb);
	}
*/
	
	



}
