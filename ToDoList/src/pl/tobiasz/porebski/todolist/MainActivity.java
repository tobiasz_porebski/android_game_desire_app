package pl.tobiasz.porebski.todolist;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import pl.tobiasz.porebski.database.TasksDbHelper;
import pl.tobiasz.porebski.database.TableHelper.TableSchema;

public class MainActivity extends Activity {

	private MyCursorAdapter tasksAdapter;
	private TasksDbHelper dbHelper;
	private LinearLayout EditDeleteButtonsLayout;
	private boolean isEditDeleteButtonsLayoutVisible;
	private int originalEditDeleteButtonsLayoutHeight;
	private Context mContext;
	private View lastRowSelected;
	private long idofSelectedTsk;
	private Animation animShow,animHide;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mContext = this;
		EditDeleteButtonsLayout = (LinearLayout)findViewById(R.id.layout_buttons_act1);
		originalEditDeleteButtonsLayoutHeight = EditDeleteButtonsLayout.getHeight();
		EditDeleteButtonsLayout.setVisibility(View.GONE);
		isEditDeleteButtonsLayoutVisible = false;
		
		dbHelper = new TasksDbHelper(this);
		dbHelper.open();
		
		// For debugging
		// Clean all data
		
		dbHelper.deleteAllTasks();
		// Add some tasks
		dbHelper.fillDbWithSomeData();
		
		displayTasks();
		
	}

	
	// -----------	Menus Methods 	-----------------------------------------------------------
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	// -----------	Display Tasks 	-----------------------------------------------------------
	
	private void displayTasks(){
		
		Cursor tasksCursor = dbHelper.getAllTasksForListView();
		tasksCursor.moveToFirst();
		
		// The columns that are needed to show in listView
		String[] columns = new String[]{
				TableSchema.COLUMN_NAME_TITLE,
				TableSchema.COULUMN_NAME_TIME_END,
				TableSchema.COULUMN_NAME_ICON_URL,
				// with this app could not work - BE CAREFUL
				//TableSchema.COLUMN_NAME_TASK_ID
				
		};
		
		// the XML defined views which the data will be bound to
		int[] viewsToBind = new int[]{
				R.id.firstLine,
				R.id.secondLine,
				R.id.icon
		};
		
		//create the adapter
		tasksAdapter = new MyCursorAdapter(
				this,
				R.layout.list_adapter,
				tasksCursor,
				columns,
				viewsToBind);
		
		final ListView listView = (ListView) findViewById(R.id.list_todo);
		
		// Now assign adapter to ListView
		listView.setAdapter(tasksAdapter);
		
		// Detect Move Gesture - diselect element
		listView.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				final int action = MotionEventCompat.getActionMasked(event);
				
				switch(action){				
				case (MotionEvent.ACTION_MOVE):
					
					// show tat item is no longer selected
					if(lastRowSelected != null){
						lastRowSelected.setBackgroundColor(Color.WHITE);
					}
					lastRowSelected = null;
					
					// TODO : Hide buttons if visible
					hideBottomLayout();
//					EditDeleteButtonsLayout.setVisibility(View.GONE);
//					Log.d("DRAG", "Performed a scroll");
					
					break;
				}
				return false;
			}
		});
		// Define what will happend after clicking on the element
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				if(lastRowSelected != null){
					lastRowSelected.setBackgroundColor(Color.WHITE);
				}
				
				// Get the cursor, it will be positioned on selected item
				 Cursor cursor = (Cursor) listView.getItemAtPosition(position);
				 
				 long rowId = cursor.getLong(cursor.getColumnIndex(TableSchema.COLUMN_NAME_TASK_ID));
				 Toast.makeText(mContext, "Elemnt Id: "+ String.valueOf(rowId), Toast.LENGTH_SHORT).show();
				 
				 //set selected id
				 idofSelectedTsk = rowId;
				 
				 // Show that item was selected
				 lastRowSelected = view;
				 view.setBackgroundColor(Color.BLUE);
				 // TODO: The buttons should appear.
				 showBottomLayout();
//				 EditDeleteButtonsLayout.setVisibility(View.VISIBLE);
				 
				//Smoothely scroll
				 listView.smoothScrollToPositionFromTop(position, 200);;
				 				
			}
		});
	}
	
	private void startEditActivity(long rowId){
		
		Cursor c = dbHelper.getSpecificTaskInformation(rowId);
		c.moveToFirst();
		
		Intent intent = new Intent(this, TaskInformationActivity.class);
		
		intent.putExtra(TableSchema.COLUMN_NAME_TASK_ID, rowId);
		intent.putExtra(TableSchema.COLUMN_NAME_TITLE, c.getString(c.getColumnIndex(TableSchema.COLUMN_NAME_TITLE)));
		intent.putExtra(TableSchema.COLUMN_NAME_DESCRIPTION, c.getString(c.getColumnIndex(TableSchema.COLUMN_NAME_DESCRIPTION)));
		intent.putExtra(TableSchema.COULUMN_NAME_TIME_END, c.getLong(c.getColumnIndex(TableSchema.COULUMN_NAME_TIME_END)));
		intent.putExtra(TableSchema.COULUMN_NAME_ICON_URL, c.getString(c.getColumnIndex(TableSchema.COULUMN_NAME_ICON_URL)));
		
		startActivity(intent);
	}
	
	private void hideBottomLayout(){
		
		if(isEditDeleteButtonsLayoutVisible == false){
			return;
		}
		
		isEditDeleteButtonsLayoutVisible = false;
		
		EditDeleteButtonsLayout.animate()
	    .translationY(0)
	    .alpha(0.0f)
	    .setDuration(1000)
	    .setListener(new AnimatorListenerAdapter() {
	        @Override
	        public void onAnimationEnd(Animator animation) {
	            super.onAnimationEnd(animation);
	            EditDeleteButtonsLayout.setVisibility(View.GONE);
	        }
	    });
		
	}
	
	private void showBottomLayout(){
		
		if(isEditDeleteButtonsLayoutVisible == true){
			return;
		}
		
		isEditDeleteButtonsLayoutVisible = true;
		
		EditDeleteButtonsLayout.animate()
	    .translationY(originalEditDeleteButtonsLayoutHeight)
	    .alpha(1.0f)
	    .setDuration(1000)
	    .setListener(new AnimatorListenerAdapter() {
	        @Override
	        public void onAnimationEnd(Animator animation) {
	            super.onAnimationEnd(animation);
	            EditDeleteButtonsLayout.setVisibility(View.VISIBLE);
	        }
	    });
		
	}
//	
//	private void initAnimation()
//	{
//		animShow = AnimationUtils.loadAnimation( this, R.anim.view_show);
//		animHide = AnimationUtils.loadAnimation( this, R.anim.view_hide);
//	}
	
}
